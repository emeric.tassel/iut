#include <stdio.h>
#include <stdlib.h>

typedef char chaine20[21] ;

typedef struct {
    chaine20 nom ;
    int population ;
    int superficie ;
} t_capitale ;

typedef struct {
    chaine20 nom ;
    t_capitale *capitale ;
} t_pays ;

void init_pays(t_pays * t){
    printf("Donnez le nom du pays : ");
    scanf("%s",t->nom);
    printf("Donnez le nom de la capitale : ");
    scanf("%s",t->capitale->nom);
    printf("Donnez la population de la capitale : ");
    scanf("%d",&(t->capitale->population));
    printf("Donnez la superficie de la capitale  : ");
    scanf("%d",&(t->capitale->superficie));
}

void affiche_pays(t_pays t){
    printf("\n\n");
    printf("nom du pays : %s \n",t.nom);
    printf("nom de sa capitale : %s \n",(t.capitale)->nom);
    printf("population de sa capitale : %d \n",(t.capitale)->population);
    printf("superficie de sa capitale : %d \n",(t.capitale)->superficie);
    printf("\n\n");
}

int main()
{
    // Q1
    t_pays pp;
    pp.capitale = (t_capitale *) malloc(sizeof(t_capitale));

    init_pays(&pp);
    affiche_pays(pp);

    free(pp.capitale);

    // Q2
    t_pays *ptp;
    ptp = (t_pays *) malloc(sizeof(t_pays));
    ptp->capitale = (t_capitale *) malloc(sizeof(t_capitale));

    init_pays(ptp);
    affiche_pays(*ptp);

    free(ptp->capitale);
    free(ptp);

    return 0;
}

