#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
    // Exercice 1
    int x = 100;
    int *pt;

    pt=&x;
    *pt = *pt + 10;
    printf("x=%d (*pt)=%d \n", x, *pt);

    free(pt);

    return 0;
}
