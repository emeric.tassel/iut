#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef char chaine20[21] ;

typedef struct {
    chaine20 nom ;
    int population ;
    chaine20 capitale ;
} t_region ;

int main()
{
    // Exercice 2
    t_region P1;

    printf("Donnez le nom de la région : ");
    scanf("%s",P1.nom);
    printf("Donnez la population de la région : ");
    scanf("%d",&(P1.population));
    printf("Donnez le nom de la capitale de la région : ");
    scanf("%s",P1.capitale);

    printf("\n\nNom de la region : %s Population : %d  Capitale : %s \n\n",P1.nom,P1.population,P1.capitale);
    
    t_region *P;
    P=(t_region *)malloc(sizeof(t_region));
    strcpy(P->nom,"Bretagne");
    P->population = 60000;
    strcpy(P->capitale,"Rennes");

    printf("\n\nNom de la region : %s Population : %d  Capitale : %s \n\n",P->nom,P->population,P->capitale);

    printf("Donnez le nom de la region : ");
    scanf("%s",P->nom);
    printf("Donnez la population de la region : ");
    scanf("%d",&(P->population));
    printf("Donnez le nom de la capitale de la region : ");
    scanf("%s",P->capitale);

    printf("\n\nNom de la region : %s Population : %d  Capitale : %s \n\n",P->nom,P->population,P->capitale);

    free(P);

    return 0;
}
