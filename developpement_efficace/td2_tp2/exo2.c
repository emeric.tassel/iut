#include <stdio.h>
#include <stdlib.h>

typedef struct elem {
	int val;
	struct elem* svt;
} telement;

typedef telement *ensemble;

#define tailleTab 10
typedef int tab10[tailleTab];

// Question 1
void init_ens(ensemble *l){
    *l=NULL;
}

int appartient(ensemble l, int n){
    int res=0;
    telement * p;
    p=l;
    while((p!=NULL) && (res==0)){
        if (p->val==n) res=1;

        p=p->svt;
    }
    return res;
}

void ajouter(ensemble *l, int a){
    if (appartient(*l,a)==0){
        telement *e = (telement *)malloc(sizeof(telement));
        e->val=a;
        e->svt=*l;
        *l=e;
    }
}

void afficher_ensemble(ensemble l){
    telement * p;
    p=l;
    printf("Element de l'ensemble : \n");
    while(p!=NULL){
        printf("%d ",p->val);
        p=p->svt;
    }
    printf("\n");
    
}

// Question 2
void ajouter_de_tab(ensemble *l,tab10 tab){
    int i;
   
    for (i=0;i<tailleTab;i++){
        ajouter(l,tab[i]);
    }   
}

// Question 3
int cardinal(ensemble l){
    int res=0 ;
    telement *parcours ;
    
    parcours=l ;
    while (parcours !=NULL){
        res++ ;
        parcours=parcours->svt ;
    }

    return res;
}

// Question 4

void union_ens(ensemble l1, ensemble l2, ensemble *res){
    telement * parcours;
    init_ens(res);

    parcours=l1;
    while (parcours !=NULL){
        ajouter(res,parcours->val) ;
        parcours=parcours->svt ;
    }

    parcours=l2;
    while (parcours !=NULL){
        ajouter(res,parcours->val) ;
        parcours=parcours->svt ;
    }
}

// Question 5
void inter_ens(ensemble l1, ensemble l2, ensemble *res){
    telement * parcours;
    init_ens(res);
    parcours=l1;
    while (parcours !=NULL){
        if (appartient(l2,parcours->val) !=0){
            ajouter(res,parcours->val) ;
        }

        parcours=parcours->svt ;
    }
}



int main()
{
    ensemble ens1;
    ensemble ens2;
    ensemble ens3;

    tab10 tab1={0,1,2,2,3,4,1,0,5,6};
    tab10 tab2={1,2,2,3,4,1,5,6,7,8};
    init_ens(&ens1);
    init_ens(&ens2);

	ajouter(&ens1, 5);
	afficher_ensemble(ens1);

	if(appartient(ens1, 5) > 0) {
		printf("5 est dans l'ensemble\n");
	}

    ajouter_de_tab(&ens1,tab1);
    ajouter_de_tab(&ens2,tab2);
    printf("\n voici ens1 : ");
    afficher_ensemble(ens1);
    printf("\n voici ens2 : ");
    afficher_ensemble(ens2);
    union_ens(ens1,ens2,&ens3);
    printf("\n voici ens1 union ens2 : ");
    afficher_ensemble(ens3);
    inter_ens(ens1,ens2,&ens3);
    printf("\n voici ens1 inter ens2 : ");
    afficher_ensemble(ens3);

    printf("\n cardinal de ens1 = %d \n", cardinal(ens1));

    free(ens1);
    free(ens2);
    free(ens3);

    return 0;
}
