#include <stdio.h>
#include <stdlib.h>

typedef struct elem {
	struct elem* prec;
	int val;
	struct elem* svt;
} telement;

typedef telement* tlisteDC;

void init(tlisteDC *l){
    *l=NULL;
}

void inserer_en_tete(tlisteDC *l, int a){
    telement *e = (telement *)malloc(sizeof(telement));
    e->val=a;
    e->prec=NULL;
    e->svt=*l;
    if ((*l)!=NULL){
        (*l)->prec=e;
    }
    *l=e;
}


void afficher(tlisteDC l){
    telement * p;
    p=l;
    printf("\n");
    while(p!=NULL){
        printf("%d ",p->val);
        p=p->svt;
    }
}

void inserer_apres(tlisteDC *l, int a, int I){
    telement *e;
    telement*parcours;

    int cpt=1;
    if ((*l)==NULL){
        inserer_en_tete(l,a);
    }else{
        parcours=*l;
        while ((parcours!=NULL) && (cpt!=I)){
            parcours=parcours->svt;
            cpt++;
        }
         if (parcours!=NULL){
            e = (telement *)malloc(sizeof(telement));
            e->val=a;
            e->svt=parcours->svt;
            e->prec=parcours;
            if ((parcours->svt)!=NULL) parcours->svt->prec=e;
            parcours->svt=e;

         }
    }
}

int main()
{
    tlisteDC maliste;
    init(&maliste);
    inserer_en_tete(&maliste,5);
    inserer_en_tete(&maliste,3);
    inserer_en_tete(&maliste,2);
    inserer_en_tete(&maliste,0);

    inserer_apres(&maliste,1,1);
    inserer_apres(&maliste,4,4);
    inserer_apres(&maliste,6,6);
    afficher(maliste);

    return 0;
}
