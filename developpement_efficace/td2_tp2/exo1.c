#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Exercice 1
typedef char chaine[50];

// 1. les définitions des types personne (tpersonne), élément (telement) et liste (tliste) ;
typedef struct {
	chaine nom;
	int age;
} tpersonne ;

typedef struct elem {
	tpersonne pers;
	struct elem* svt;
} telement;

typedef telement *tliste;

/**
 * 2. la procédure d’initialisation d’une tliste l ;
 */ 
void init_liste(tliste *l){
    *l=NULL;
}

/**
 * 3. la procédure d’insertion d'une nouvelle personne (nom et age) en tête d'une tliste l;
 */
void insertion_en_tete(tliste *l, chaine n, int a){
    telement *e = (telement *)malloc(sizeof(telement));

    strcpy(e->pers.nom,n);
    e->pers.age=a;
    e->svt=*l;
    *l=e;
}

/**
 * 4. la procédure d’affichage à l’écran des éléments d’une tliste l (pensez à définir une procédure
pour afficher une tpersonne) ;
 */
void affiche_personne(tpersonne p){
    printf("Nom: %s - Age: %d \n",p.nom, p.age);
}

void afficher_liste(tliste l){
    printf("Contenu de la liste \n");
	
    if(l == NULL) {
        printf("Liste vide \n");
    }else{
        tliste p;
        p=l;
        while(p!=NULL){
            affiche_personne(p->pers);
            p=p->svt;
        }
    }
}

/**
 * 5. La fonction de recherche d'une personne à partir de son nom dans une tliste l, retournant
 * l'adresse de l'élément où elle se trouve dans la liste si la personne est présente, NULL sinon.
 */
telement * existe(tliste l, chaine leNom){
    telement * res=NULL;
    telement *parcours;
    parcours=l;

    while ((res == NULL) && (parcours != NULL)){
        if (strcmp(parcours->pers.nom,leNom) == 0)  res=parcours;
    
        parcours=parcours->svt;
    }

    return res;
}

void modifer_liste(tliste l, chaine leNom1, chaine leNom2){
    tliste p;
    p=l;
    
    while(p!=NULL){
        if (strcmp(p->pers.nom,leNom1)== 0)  {
            strcpy(p->pers.nom,leNom2);
        }

        p=p->svt;
    }
}

/**
 * 6. La fonction supprimant le telement en tête d'une tliste l.
 */ 
void supprimer_tete(tliste *l){
    telement * p;
    if (*l!=NULL){
        p=*l;
        *l=(*l)->svt;
        
        free(p);
    }
}

/**
 * 7. Écrire une fonction permettant de compter le nombre de personnes d'un même nom dans une
 * tliste l. Il sera judicieux d'utiliser la fonction de recherche de la 5e question.
 */
int compteNom(tliste l, char * leNom){
    int res=0;
    telement * clui;

    clui=existe(l, leNom);
    while (clui != NULL){
        res++ ;
        clui=existe(clui->svt, leNom);
    }
    
    return res ;
}

/**
 * 8. Écrire une fonction permettant de rechercher le ième telement dans une tliste l et de retourner
 * son adresse, ou NULL si la liste contient moins de i telement.
 */
telement * leNumeroI(tliste l, int i){
    telement * parcours ;
    int cpt = 1;
    parcours=l ;
    while ((parcours !=NULL) && (cpt<i)){
        cpt++;
        parcours=parcours->svt;
    }
    return parcours;
}

/**
 * 9. Une fonction pour insérer une personne en fin de liste.
 */
void insertion_en_fin(tliste *l, chaine n, int a){
    telement *e = (telement *)malloc(sizeof(telement));
    telement *parcours;

    strcpy(e->pers.nom,n);
    e->pers.age=a;
    e->svt=NULL;
    if ((*l)==NULL){
        *l=e;
    }else{
      parcours=*l;
      while (parcours->svt!=NULL){
        parcours=parcours->svt;
      }
      parcours->svt=e;
    }
}

/**
 * 10. Proposez une procédure pour insérer une personne après le Iième élément de la liste (on ne fait
 * rien si la liste ne comporte pas de Ième élément).
 */
void insertion_apres(tliste l, chaine n, int a, int I){
    telement *e;
    telement *parcours;

    int cpt=1;

    parcours=l;
    while ((parcours!=NULL) && (cpt!=I)){
        parcours=parcours->svt;
        cpt++;
    }
    if (parcours!=NULL){
        e = (telement *)malloc(sizeof(telement));
        strcpy(e->pers.nom,n);
        e->pers.age=a;
        e->svt=parcours->svt;
        parcours->svt=e;
    }else{
        printf("Pas de place pour inserer l'element a la position %d\n", I);
    }
}

// Méthode pour nettoyer une liste proprement
void nettoyer_liste(tliste *l) {
    while(*l != NULL) {
        printf("Suppression %s\n", (*l)->pers.nom);
        supprimer_tete(l);
    }
}


int main(){
    chaine nom;
    int age;
    tliste maliste;
    telement * pt;


    init_liste(&maliste);
    insertion_en_tete(&maliste,"Test1",25);
    insertion_en_fin(&maliste,"Test2",26);
    insertion_en_fin(&maliste,"Test3",27);
    insertion_en_fin(&maliste,"Test4",28);
    insertion_en_fin(&maliste,"Test5",29);
    //modifer_liste(maliste, "Test2", "test");
    afficher_liste(maliste);
    printf("donnez un nom (-1 pour terminer) exactement 5\n");
    scanf("%s",nom);
    while (strcmp(nom,"-1")!=0){
        printf("donnez un age \n");
        scanf("%d",&age);
        insertion_en_fin(&maliste,nom,age);
        printf("donnez un nom (-1 pour terminer)\n");
        scanf("%s",nom);
    }
    printf("\n-----------------------------------------\n");
	
    insertion_apres(maliste,"Marcel",7,5);
    printf("Marcel est le six (dernier)\n");
    afficher_liste(maliste);
    
    printf("\n-----------------------------------------\n");
	
    insertion_apres(maliste,"Michel",7,1);
    printf("Michel est le deuxieme\n");
    afficher_liste(maliste);
    
    printf("\n-----------------------------------------\n");
	
    insertion_apres(maliste,"Monique",7,50);
    printf("Monique n'y est pas\n");
    afficher_liste(maliste);
    
    printf("\n-----------------------------------------\n");
	
    insertion_apres(maliste,"Maurice",7,3);
    printf("Maurice est le quatrieme\n");
    afficher_liste(maliste);

    printf("\n-----------------------------------------\n");
	

    if (existe(maliste,"Michel")==NULL) printf("\nMichel est absent \n");
    if (existe(maliste,"Maurice")!=NULL) printf("\nMaurice est present \n");

    supprimer_tete(&maliste);
    afficher_liste(maliste);

    printf("\n-----------------------------------------\n");

    printf("Nombre de Maurice %d\n", compteNom(maliste,"Maurice"));
    pt=leNumeroI(maliste,2);
    printf("%s est le numero 2\n", pt->pers.nom);
    pt=leNumeroI(maliste,20);
    if (pt==NULL) printf("pas de numero 20\n");

    printf("\n-----------------------------------------\n");
    nettoyer_liste(&maliste);

    printf("\n-----------------------------------------\n");

    afficher_liste(maliste);

    return 0;
}









